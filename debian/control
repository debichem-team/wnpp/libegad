Source: libegad
Section: science
Priority: extra
Maintainer: LI Daobing <lidaobing@gmail.com>
Build-Depends: debhelper (>= 5), dpatch, zlib1g-dev, libtool
Standards-Version: 3.7.3
Homepage: http://egad.ucsd.edu/egad/

Package: libegad0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}
Description: rational protein design library
 The EGAD Library is a modular, object oriented approach to rational protein
 design. It is platform-independent, written in C++ and, most importantly,
 free. Its raison d'etre is to serve as a tool for building protein design
 applications. It can also be used as a test-bed for the comparison of
 different energy functions and minimization algorithms under the same physical
 model.
 .
 Citations of EGAD Library should reference our paper in the Journal of
 Computational Chemistry. doi:10.1002/jcc.20727

Package: libegad-dbg
Section: libdevel
Architecture: any
Depends: libegad0 (= ${binary:Version})
Description: rational protein design library (debugging symbols)
 The EGAD Library is a modular, object oriented approach to rational protein
 design. It is platform-independent, written in C++ and, most importantly,
 free. Its raison d'etre is to serve as a tool for building protein design
 applications. It can also be used as a test-bed for the comparison of
 different energy functions and minimization algorithms under the same physical
 model.
 .
 Citations of EGAD Library should reference our paper in the Journal of
 Computational Chemistry. doi:10.1002/jcc.20727
 .
 This package contains detached debugging symbols.

Package: libegad-dev
Section: libdevel
Architecture: any
Depends: libegad0 (= ${binary:Version}), zlib1g-dev
Description: rational protein design library (development files)
 The EGAD Library is a modular, object oriented approach to rational protein
 design. It is platform-independent, written in C++ and, most importantly,
 free. Its raison d'etre is to serve as a tool for building protein design
 applications. It can also be used as a test-bed for the comparison of
 different energy functions and minimization algorithms under the same physical
 model.
 .
 Citations of EGAD Library should reference our paper in the Journal of
 Computational Chemistry. doi:10.1002/jcc.20727
 .
 This package includes the static library and the header files.

Package: libegad-doc
Section: doc
Architecture: all
Description: Documentation for libegad (documentation)
 The EGAD Library is a modular, object oriented approach to rational protein
 design. It is platform-independent, written in C++ and, most importantly,
 free. Its raison d'etre is to serve as a tool for building protein design
 applications. It can also be used as a test-bed for the comparison of
 different energy functions and minimization algorithms under the same physical
 model.
 .
 Citations of EGAD Library should reference our paper in the Journal of
 Computational Chemistry. doi:10.1002/jcc.20727
 .
 This package includes the library documentation.
